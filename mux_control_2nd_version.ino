//FUNCTION HEADER
  void setup_pin();
  void setup_values();
  void WriteChannel(int *Port, int channel);
  void portStatus();
//GLOBAL VARIABLES

//PIN POSITIONS (ARDUINO HARDWARE PINS)
  int P_positions[8]={23,25,27,29,31,33,35,37};//A0 A1 A2 A3 A4 EN WR CS
//LOGIC OUTPUT VLAUES
  int L_values[8]={0,0,0,0,0,//A0 A1 A2 A3 A4 
                    0,0,0};// EN WR CS
//LOOP VARIABLES
  String Pin_values("");
  int count=0;
  int converter=0;
//SETUP FUNCTION
void setup() {

  //OUTPUT PINS AVAILABLE
    setup_pin();
  //LOGIC VALUES THAT WILL BE WRITTEN IN EVERY PIN
    WriteChannel(L_values,0);
  //LOGIC VALUES WRITING 
    setup_values();
  //COMMUNICATION RATE
    Serial.begin(9600);

}

//LOOP FUNCTION
void loop() {
  
    if (Serial.available()){
      Pin_values=Serial.readStringUntil('\n'); //STORAGE THE VALUE
        //SOFTWARE AND HARDWARE 
        //THEN DO THE PRINT
        Serial.println("Channel selected: "+Pin_values); //ARGUMENT
        //CHAR TO INT AND MULTIPLEXER SYNTAX (1-32 CHANNEL)
        converter = atof(Pin_values.c_str());
        WriteChannel(L_values,converter-1);//SOFTWARE WRITING 
        setup_values();//HARDWARE WRITING
    }
    }

//FUNCTION DEFINITION
//FUNCTION TO WIRITE THE LOGIC VALUES FOR THE CHANNEL SELECTION
void WriteChannel(int *Port, int channel){
  //WRITE LOGIC VALUES IN THE ARRAGE
  for(int n=0;n<5;n++){
    Port[n] = channel%2;
    channel = channel/2;
  }
}
// PRINT THE NEW PORT AFTER DOING SOME KIND OF SETTING
void portStatus(){
  Serial.print ("new port state: ");
  Serial.print (L_values[4]);
  Serial.print (L_values[3]);
  Serial.print (L_values[2]);
  Serial.print (L_values[1]);
  Serial.print (L_values[0]);
  Serial.print ("(A4 A3 A2 A1 A0)  ");
  Serial.print (L_values[5]);
  Serial.print (L_values[6]);
  Serial.print (L_values[7]);
  Serial.println ("(EN WR CS)");
}

void setup_pin(){
  //PIN OUT 
  pinMode(P_positions[0],OUTPUT);
  pinMode(P_positions[1],OUTPUT);
  pinMode(P_positions[2],OUTPUT);
  pinMode(P_positions[3],OUTPUT);
  pinMode(P_positions[4],OUTPUT);
  pinMode(P_positions[5],OUTPUT);
  pinMode(P_positions[6],OUTPUT);
  pinMode(P_positions[7],OUTPUT);
}
void setup_values(){
  //pIN VALUES
  digitalWrite(P_positions[0],L_values[0]);
  digitalWrite(P_positions[1],L_values[1]);
  digitalWrite(P_positions[2],L_values[2]);
  digitalWrite(P_positions[3],L_values[3]);
  digitalWrite(P_positions[4],L_values[4]);
  digitalWrite(P_positions[5],L_values[5]);
  digitalWrite(P_positions[6],L_values[6]);
  digitalWrite(P_positions[7],L_values[7]);

}
