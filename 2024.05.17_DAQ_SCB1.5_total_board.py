#!/usr/bin/env python
#imports
import Keithley
import argparse
import HAMEG4040
import time
import SerialCom
import argparse

#Run code
#python 2024.05.17_DAQ_SCB1.5_total_board.py  -k /dev/ttyUSB0 -kb /dev/ttyUSB1 -kc /dev/ttyUSB2 -r /dev/ttyACM0 -a /dev/ttyACM1 -o 2024.05.23_DAQ_report.txt


#FUNCTIONS
#VARIABLES
#measurements dynamic (current value on the point measured)
IoutD=0
VdD=0
VoutD=0
increase = 0.40
#varaiables with initial states
Vin = 3.2 #2
forward= True
up = True
ch=0
#Channel Map
MuxMap=["4","5","8","9","12","13","14","3","6","7","10","11","27","26","23","22","19","18","17","28","25","24","21","20"]
#Key for the Map [0 to 24] every channel
Current_Ch=0
#communication 
parser = argparse.ArgumentParser()
parser.add_argument("-k","--keithley",required=True)# current Iout
parser.add_argument("-r","--rohde",required=True)# VAP -VAP Vin
parser.add_argument("-a","--arduino",required=True)# arduino
parser.add_argument("-kb","--keithleyb",required=True)#Vout
parser.add_argument("-kc","--keithleyc",required=True)#Vd
parser.add_argument("-o","--output",required=True)#output file

args = parser.parse_args()

ardui = SerialCom.SerialCom(args.arduino,9600,timeout=1)#arduino
hameg = HAMEG4040.HAMEG4040(args.rohde)# VAP -VAP Vin
keith = Keithley.Keithley(args.keithley)# current Iout
keithb = Keithley.Keithley(args.keithleyb)# Vout
keithc = Keithley.Keithley(args.keithleyc)# Vd

#GRAPHS

#MAIN PROGRAM

# 1. Enable Power supplies and data report file
print("enable power to SCB")
hameg.setVoltage(3,13.5)
hameg.enableOutput(3,True)
hameg.setVoltage(4,13.5)
hameg.enableOutput(4,True)
hameg.setVoltage(2,Vin)
hameg.enableOutput(2,True)
fw=open(args.output,"w")#w+

# 2. Loop for channel chage
for rep in range(24):
	# 3. Read Channel
	forward = True
	ret=ardui.writeAndRead(MuxMap[Current_Ch])
	print("::%s" % ret)
	time.sleep(15)
	# 4. Start the channel sweep
	while forward:
		# checks if Vin is higher than 10
		if (Vin <= 7.6) and (Vin >=3.2):#!!!!!!!!!!!!!!!!!!!!!!! CHANGE 7.3 BY 10 LATER
			for rep in range(3):
				hameg.setVoltage(2,Vin)
				time.sleep(1)	
				#actual value		
				ch = int(MuxMap[Current_Ch])
				VdD=keithc.getVoltage()
				VoutD=keithb.getVoltage()
				IoutD=keith.getCurrent()*1000000
				ss="%02i,%.3f,%.2f,%.3f,%.5f" % (int(MuxMap[Current_Ch]),Vin,IoutD,VoutD,VdD)
				print(ss)
				fw.write("%s\n" % ss)
			pass
			
			if up:
				Vin+=increase
			else:
				Vin-=increase
			
		else: 
			Current_Ch+=1
			up=not up
			forward = False
			if up:
				Vin+=increase
			else:
				Vin-=increase
			pass
		pass
		
pass



